abstract class Operacion {
	double n1;
	double n2;
	double res;
	String operacion;

	Operacion(double n1, double n2, String operacion) {
		this.n1 = n1;
		this.n2 = n2;
		this.operacion = operacion;
	}

	protected void setRes(double res) {
		this.res = res;
	}
}