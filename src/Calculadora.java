/**
 * Clase calculadora
 * @author Luis
 * @version 1.0
 */

class Calculadora {

	static int n1 = 0;
	static int n2 = 0;
	
  public static void main(String[] args) {

	  	n1 = new Integer (args[0]);
	  	n2 = new Integer (args[1]);
	  
        //suma
        Suma sum = new Suma(n1,n2);
        sum.mostrarResultado();
        
        //resta
        Resta res = new Resta(n1,n2);
        res.mostrarResultado();
        
        //multiplicacion
        Multiplicacion mul = new Multiplicacion(n1,n2);
        mul.mostrarResultado();
        
        //division
        Division div = new Division(n1,n2);
        div.mostrarResultado();
        
        //raiz
        Raiz raiz = new Raiz(n1,n2);
        raiz.mostrarResultado();
        
        //cuadrado
        Cuadrado p2 = new Cuadrado(n1,n2);
        p2.mostrarResultado();
        
        //cubo
        Cubo p3 = new Cubo(n1,n2);
        p3.mostrarResultado();
        
        return;
   }
 }