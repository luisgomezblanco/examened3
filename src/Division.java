/**
 * Clase Divisi�n
 * @version 1.2
 * @author Luis
 *
 */

public class Division extends Operacion{
    
    double div = 0;
       
    public Division(double n1, double n2) {
             
        super(n1, n2, "/");
        if(n2==0) System.out.println("no se puede division entre cero");
        else this.div = n1 / n2;
        this.setRes(this.div);
    }
    
    public void mostrarResultado() {
      System.out.println(this.n1+ " " + this.operacion + " " + this.n2 +" = " + this.res);
    } 
}