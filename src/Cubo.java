public class Cubo extends Operacion{
    
    double pow3 = 0;
       
    public Cubo(double n1, double n2) {
             
        super(n1, n2, " ^3 ");
        this.pow3 = n1*n1*n1;
        this.setRes(this.pow3);
    }

    public void mostrarResultado() {
      System.out.println(this.n1+this.operacion+"= "+this.res);
    } 
    
}