public class Raiz extends Operacion{
    
    double sqrt = 0;
       
    public Raiz(double n1, double n2) {
             
        super(n1, n2, " SQRT ");
        this.sqrt = Math.sqrt(n1);
        this.setRes(this.sqrt);
    }
    
    public void mostrarResultado() {
      System.out.println(this.operacion+" "+this.n1+" = "+this.res);
    } 
}