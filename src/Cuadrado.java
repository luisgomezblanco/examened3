/**
 * Clase Cuadrado
 * @author Luis
 * @version 1.0
 */

public class Cuadrado extends Operacion{
    
    double pow2 = 0;
       
    public Cuadrado(double n1, double n2) {
             
        super(n1, n2, " ^2 ");
        this.pow2 = n1*n1;
        this.setRes(this.pow2);
    }
    
    public void mostrarResultado() {
      System.out.println(this.n1+this.operacion+"= "+this.res);
    } 
}